﻿using DapperDb.DAL;
using System.Configuration;

namespace DapperDb
{
	class Program
	{
		static void Main(string[] args)
		{
			var service = new DatabaseService(ConfigurationManager.ConnectionStrings["mssql"].ConnectionString);
			var products = service.GetProducts();
			var employees = service.GetEmployees();
			var regions = service.GetRegions();
			var history = service.GetHistories();

			service.UpdateOrder(28,2);
		}
	}
}