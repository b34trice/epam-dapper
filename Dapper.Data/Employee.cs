﻿using DapperDb.Entities;
using System.Collections.Generic;

namespace DapperDb.Data
{
	public class Employee
	{
		public DbEmployee EmployeeData { get; set; }

		public List<DbRegion> Regions { get; set; }

		public List<DbTerritory> Territories { get; set; }
	}
}