﻿using DapperDb.Entities;
using System.Collections.Generic;

namespace DapperDb.Data
{
	public class History
	{
		public DbEmployee Employee { get; set; }

		public List<DbShipper> ShipCompanies { get; set; }
	}
}
