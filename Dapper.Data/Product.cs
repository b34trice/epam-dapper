﻿using DapperDb.Entities;

namespace DapperDb.Data
{
	public class Product
	{
		public DbSupplier Supplier { get; set; }

		public DbCategory Category { get; set; }

		public DbProduct ProductData { get; set; }
	}
}