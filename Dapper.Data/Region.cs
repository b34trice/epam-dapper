﻿namespace DapperDb.Data
{
	public class Region
	{
		public string RegionName { get; set; }

		public int EmployeesCounter { get; set; }
	}
}