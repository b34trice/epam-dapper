using DapperDb.DAL;
using DapperDb.Entities;
using System.Collections.Generic;
using System.Configuration;
using Xunit;

namespace DatabaseServiceUnitTests
{
	public class DatabaseServiceUnitTest
	{
		private readonly DatabaseService _service;

		public DatabaseServiceUnitTest()
		{
			_service = new DatabaseService(ConfigurationManager.ConnectionStrings["mssql"].ConnectionString);
		}

		[Fact]
		public void GetHistories_GetData()
		{
			_service.GetHistories();
		}

		[Fact]
		public void GetProducts_GetData()
		{
			_service.GetProducts();
		}

		[Fact]
		public void GetRegions_GetData()
		{
			_service.GetRegions();
		}

		[Fact]
		public void GetEmployees_GetData()
		{
			_service.GetEmployees();
		}

		[Fact]
		public void AddNewEmployeeWithTerritory_AddData()
		{
			var employee = new DbEmployee { LastName = "test", FirstName = "test"};
			var territories = new List<DbEmployeeTerritory>();
			_service.AddNewEmployeeWithTerritory(employee, territories);
		}

		[Fact]
		public void UpdateCategoty_UpateData()
		{
			var product = new DbProduct { ProductId = 1, CategoryId = 2 };
			_service.UpdateCategory(product);
		}

		[Fact]
		public void AddProduct_InsertProducts()
		{
			var suplier = new DbSupplier { CompanyName = string.Empty };
			var category = new DbCategory { CategoryName = string.Empty };

			_service.AddProducts(new List<DbProduct> { new DbProduct { ProductName = "test" } }, suplier, category);
		}

		[Fact]
		public void UpdateOrder_UpdateData()
		{
			_service.UpdateOrder(12, 5);
		}
	}
}