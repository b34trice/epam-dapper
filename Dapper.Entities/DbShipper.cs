﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DapperDb.Entities
{
	[Table("Shippers")]

	public class DbShipper
	{
		[Key]
		public int ShipperId { get; set; }

		public string CompanyName { get; set; }

		public string Phone { get; set; }
	}
}
