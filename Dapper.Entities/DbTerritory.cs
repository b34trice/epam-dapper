﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DapperDb.Entities
{
	[Table("Territories")]

	public class DbTerritory
	{
		[Key]
		public int TerritoryId { get; set; }

		public string TerritoryDescription { get; set; }

		public int RegionId { get; set; }
	}
}
