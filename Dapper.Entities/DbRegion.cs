﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DapperDb.Entities
{
	[Table("Region")]

	public class DbRegion
	{
		[Key]
		public int RegionId { get; set; }

		public string RegionDescription { get; set; }
	}
}