﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DapperDb.Entities
{
	[Table("EmployeeTerritories")]

	public class DbEmployeeTerritory
	{
		public int EmployeeId { get; set; }

		public int TerritoryId { get; set; }
	}
}