﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DapperDb.Entities
{
	[Table("Products")]

	public class DbProduct
	{
		[Key]
		public int ProductId { get; set; }

		public string ProductName { get; set; }

		public int SupplierId { get; set; }

		public int CategoryId { get; set; }

		public string QuantityPerUnit { get; set; }

		public decimal UnitPrice { get; set; }

		public short UnitsInStock { get; set; }

		public short UnitsOnOrder { get; set; }

		public bool Descontinued { get; set; }
	}
}