﻿using System.Collections.Generic;
using DapperDb.Data;
using Dapper;
using System.Data.SqlClient;
using System.Linq;
using DapperDb.Entities;
using Dapper.Contrib.Extensions;

namespace DapperDb.DAL
{
	public class DatabaseService
	{
		private readonly string _connectionString;

		public DatabaseService(string connectionString)
		{
			_connectionString = connectionString;
		}

		public IEnumerable<History> GetHistories()
		{
			var result = new List<History>();
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				var employees = connection.Query<DbEmployee>($"SELECT * FROM Employees", null, transaction);
				foreach (var employee in employees)
				{
					var shippers = new List<DbShipper>();
					var orders = connection.Query<DbOrder>($"SELECT DISTINCT ShipVia FROM Orders WHERE EmployeeId = {employee.EmployeeId}", null, transaction);
					foreach (var order in orders)
					{
						shippers.Add(connection.Query<DbShipper>($"SELECT * From Shippers WHERE ShipperId = {order.ShipVia}", null, transaction).FirstOrDefault());
					}
					result.Add(new History { Employee = employee, ShipCompanies = shippers });
				}

				transaction.Commit();
			}
			return result;
		}

		public IEnumerable<Product> GetProducts()
		{
			var result = new List<Product>();
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				var products = connection.Query<DbProduct>("SELECT * FROM Products", null, transaction);
				foreach (var product in products)
				{
					var category = connection.Query<DbCategory>($"SELECT * FROM Categories WHERE CategoryId = {product.CategoryId}", null, transaction).FirstOrDefault();
					var supplier = connection.Query<DbSupplier>($"SELECT * FROM Suppliers WHERE SupplierId = {product.SupplierId}", null, transaction).FirstOrDefault();
					result.Add(new Product { Supplier = supplier, ProductData = product, Category = category });
				}
				transaction.Commit();
			}
			return result;
		}

		public IEnumerable<Region> GetRegions()
		{
			var result = new List<Region>();
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				var regions = connection.Query<DbRegion>($"SELECT * FROM Region", null, transaction);
				foreach (var region in regions)
				{
					var resultTerritories = new Dictionary<int, int>();
					var territories = connection.Query<DbTerritory>($"SELECT * FROM Territories WHERE RegionId = {region.RegionId}", null, transaction);
					foreach (var territory in territories)
					{
						var employeeTerritories = connection.Query<DbEmployeeTerritory>($"SELECT * FROM EmployeeTerritories WHERE TerritoryId = {territory.TerritoryId} ", null, transaction);
						foreach (var employeeTerritory in employeeTerritories)
						{
							if (resultTerritories.ContainsKey(employeeTerritory.EmployeeId))
							{
								resultTerritories[employeeTerritory.EmployeeId] += 1;
							}
							else
							{
								resultTerritories.Add(employeeTerritory.EmployeeId, 1);
							}
						}
					}
					result.Add(new Region { RegionName = region.RegionDescription, EmployeesCounter = GetEmployeesCounter(resultTerritories) });
				}
				transaction.Commit();
			}
			return result;
		}

		public IEnumerable<Employee> GetEmployees()
		{
			var result = new List<Employee>();
			using (var connection = new SqlConnection(_connectionString))
			{

				connection.Open();
				var transaction = connection.BeginTransaction();
				var employees = connection.Query<DbEmployee>($"SELECT * FROM Employees", null, transaction);
				foreach (var employee in employees)
				{
					var regions = new List<DbRegion>();
					var territories = new List<DbTerritory>();

					var employeeTerritories = connection.Query<DbEmployeeTerritory>($"SELECT * FROM EmployeeTerritories WHERE EmployeeId = {employee.EmployeeId}", null, transaction);
					foreach (var employeeTeritory in employeeTerritories)
					{
						territories = connection.Query<DbTerritory>($"SELECT * FROM Territories WHERE TerritoryId = {employeeTeritory.TerritoryId}", null, transaction).ToList();
						foreach (var territory in territories)
						{
							regions.Add(connection.Query<DbRegion>($"SELECT * FROM Region WHERE RegionId = {territory.RegionId}", null, transaction).FirstOrDefault());
						}
					}
					result.Add(new Employee { Regions = regions, Territories = territories, EmployeeData = employee });
				}
				transaction.Commit();
				return result;
			}
		}

		public void AddNewEmployeeWithTerritory(DbEmployee employee, List<DbEmployeeTerritory> territories)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				var employeeId = (int)connection.Insert(employee, transaction);
				foreach (var territory in territories)
				{
					territory.EmployeeId = employeeId;
					connection.Insert(territory, transaction);
				}
				transaction.Commit();
			}
		}

		public void UpdateCategory(DbProduct product)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				connection.Query($"UPDATE Products SET CategoryId = {product.CategoryId} WHERE ProductId = {product.ProductId}",null,transaction);
				transaction.Commit();
			}
		}

		public void AddProducts(List<DbProduct> products, DbSupplier supplier, DbCategory category)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();

				int supplierId;
				int categotyId;

				var supplierDefault = connection.Query<DbSupplier>($"SELECT * FROM Suppliers WHERE CompanyName = '{supplier.CompanyName}'",null,transaction).FirstOrDefault();
				supplierId = supplierDefault == null ? (int)connection.Insert(supplier, transaction) : supplierDefault.SupplierId;

				var categotyDefault = connection.Query<DbCategory>($"SELECT * FROM Categories WHERE CategoryName = '{category.CategoryName}'", null, transaction).FirstOrDefault();
				categotyId = categotyDefault == null ? (int)connection.Insert(category, transaction) : categotyDefault.CategoryId;

				foreach (var product in products)
				{
					product.SupplierId = supplierId;
					product.CategoryId = categotyId;
					connection.Insert(product,transaction);
				}
				transaction.Commit();
			}
		}

		public void UpdateOrder(int oldProduct,int newProduct)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();

				var orders = connection.Query<DbOrder>($"SELECT * From Orders WHERE ShippedDate is NULL", null, transaction);
				foreach (var order in orders)
				{
					connection.Query($"UPDATE [Order Details] Set ProductId = {newProduct} WHERE OrderId = {order.OrderId} AND ProductId = {oldProduct}", null, transaction);
				}

				transaction.Commit();
			}
		}		

		private int GetEmployeesCounter(Dictionary<int, int> employees)
		{
			var result = 0;
			foreach (var employee in employees)
			{
				result += employee.Value;
			}
			return result;
		}
	}
}